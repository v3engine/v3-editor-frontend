angular.module('v3-editor.directives').directive('odVector3', [
  function() {
    return {
      templateUrl: 'views/directives/od-vector3.html',
      scope: {
        ngModel: '='
      },
      link: function($scope, element, attrs){
      	$scope.label = attrs.label;
      }
    };
  }
]);
