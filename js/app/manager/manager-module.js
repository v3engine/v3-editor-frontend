'use strict';

angular.module('v3-editor.manager', [])
.config(function($stateProvider, $urlRouterProvider){
	$urlRouterProvider.otherwise("/manager");
	$stateProvider
		.state('applications_index', {
			url: '/manager',
			templateUrl: 'views/manager/index.html',
			controller: 'ApplicationManagerCtrl'
		})
		.state('application_new', {
			url: '/application_new',
			templateUrl: 'views/manager/new_app.html',
			controller: 'NewApplicationCtrl'
		});
});
