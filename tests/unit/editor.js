describe('editor', function(){
	beforeEach(module('v3-editor'));

	beforeEach(inject(function(_Editor_){
		Editor = _Editor_;
		Editor.init();
	}));

	it('settings shold be default', function(){
		// Editor.setting..s
	});

	it('should initialize applications with ID', function(){
		var appData = {
			name: "test"
		};
		var application = Editor.newApplication(appData);
		expect(application.id).toBeDefined();
	})

	it('should increase nextApplicationID', function(){
		var appData = {
			name: "test"
		};
		var application = Editor.newApplication(appData);
		Editor.init();
		expect(Editor.settings.nextApplicationID).toBe(1);
	});

	// it('should load settings', function(){
	// 	var testSettings = {
	// 		asdasd: 'asdasd'
	// 	};
	// 	localStorage.setItem('V3.settings', testSettings);
	// 	Editor.init();
	// 	expect(Editor.settings).toBe(testSettings);
	// 	console.log(Editor.settings);
	// });
});
