'use strict';

angular.module('v3-editor.directives')
.directive('v3Navigator', ['Editor',
  function(Editor) {
    return {
      templateUrl: 'views/navigator.html',
      controller: ($scope) => {
        $scope.toggleObject = (object) => {
          Editor.selectObject(object);
        };
        $scope.removeObject = (object) => {
          Editor.removeObject(object);
          Editor.save();
        }
      },
      link: ($scope, element, attrs) => {
        $scope.navType = 'objects';
        $scope.$watch('Editor.scene', (scene) =>
          $scope.objects = Editor.scene.children
        , true);
      }
    };
  }
]);
