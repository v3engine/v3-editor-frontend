angular.module('v3-editor')
.service('EditorPrimitives', function() {
	return {
		create: function(primitiveType, params){
			var primitive, geometry, material;
      if (typeof params === 'undefined') params = {};
			switch (primitiveType) {
        case 'mesh.cube':
          geometry = new THREE.BoxGeometry(1, 1, 1);
          material = new THREE.MeshLambertMaterial({
            color: 0x00ff00
          });
          primitive = new THREE.Mesh(geometry, material);
          primitive.name = "Cube";
          primitive.receiveShadow = true;
          primitive.castShadow = true;
          break;
        case 'mesh.plane':
          geometry = new THREE.PlaneGeometry(5, 20, 32);
          material = new THREE.MeshLambertMaterial({
            color: 0x00ff00,
            side: THREE.DoubleSide
          });
          primitive = new THREE.Mesh(geometry, material);
          primitive.name = "Plane";
          primitive.receiveShadow = true;
          primitive.castShadow = true;
          break;
        case 'mesh.sphere':
          _.defaults(params, {
            radius: 500,
            widthSegments: 32,
            heightSegments: 32,
            color: 0x0000ff
          });
          console.log("creating sphere", params);
          geometry = new THREE.SphereGeometry(params.radius, params.widthSegments, params.heightSegments);
          material = new THREE.MeshBasicMaterial({
            color: params.color,
            side: THREE.DoubleSide
          });
          primitive = new THREE.Mesh(geometry, material);
          primitive.name = "Sphere";
          primitive.receiveShadow = true;
          primitive.castShadow = true;
          break;
        case 'light.ambient':
          primitive = new THREE.AmbientLight(0x555555);
          break;
        case 'light.spot':
          primitive = new THREE.SpotLight(0x555555);
          primitive.position.set(0, 0, 0);
          primitive.shadowMapWidth = 1024;
          primitive.shadowMapHeight = 1024;

          // primitive.shadowCameraNear = 1200;
          // primitive.shadowCameraFar = 2500;
          // primitive.shadowCameraFov = 50;

          primitive.shadow.bias = 0.00001;
          primitive.shadow.darkness = 10;


          primitive.shadowMapWidth = window.innerWidth;
          primitive.shadowMapHeight = window.innerHeight;

          primitive.castShadow = true;
          break;
        case 'light.directional':
          primitive = new THREE.DirectionalLight(0x555555);
          primitive.position.set(0, 0, 0);
          primitive.shadowMapWidth = 1024;
          primitive.shadowMapHeight = 1024;
          primitive.castShadow = true;
          break;
        case 'light.hemisphere':
          primitive = new THREE.HemisphereLight( 0xffffbb, 0x080820, 7 );
          primitive.color.setHSL( 0.6, 1, 0.6 );
          primitive.position.set( 0, 500, 0 );
          primitive.groundColor.setHSL( 0.095, 1, 0.75 );
          break;

        case 'camera.perspective':
          var width = 100;
          var height = 100;
          primitive = new THREE.PerspectiveCamera(45, width / height, 1, 1000);
          primitive.position.set(10, 10, 10);
      }
      return primitive;
		}
	}
});
