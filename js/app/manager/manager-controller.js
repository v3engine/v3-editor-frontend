'use strict';

angular.module('v3-editor.manager')
.controller('ApplicationManagerCtrl', [
	'$scope', 'Editor', '$location', '$rootScope', 'ngDialog',
	function($scope, Editor, $location, $rootScope, ngDialog) {
		$scope.applications = Editor.storage.getApplications();
		$rootScope.title = "Applications";

		$scope.remove = function(application, $event) {
			$event.stopPropagation();
      ngDialog.open({
        template: 'views/promptBox.html',
        data: {
          message: "Are you sure you want to delete this project?"
        }
      })
      .closePromise.then(function(data) {
        if (data.value === 1) {
          Editor.removeApplication(application);
          $scope.applications = Editor.storage.getApplications();
        }
      });
		};

		$scope.open = function(application) {
      $location.path('/application/' + application.id);
    };
	}]
);
