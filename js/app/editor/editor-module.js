'use strict';

angular.module('v3-editor', [
	'ui.router',
	'ngDialog',
	'v3-editor.manager',
	'v3-editor.storages',
	'v3-editor.directives',
	'v3-editor.terrain-generator'])

.config(function(EditorProvider, $stateProvider, localStorageServiceProvider){
	EditorProvider.setStorage('EditorLocalStorage');
	localStorageServiceProvider.setPrefix('V3');
	$stateProvider.state('application', {
		url: '/application/:app_id',
		templateUrl: 'views/editor.html',
		controller: 'EditorCtrl'
	})
})

.run(function(Editor, $location) {
	Editor.init();
  if (Editor.settings.loadLastProject && Editor.settings.lastProjectId) {
    return console.log("opening new project");
  }
})

.controller('AppCtrl', function($rootScope) {
	return $rootScope.title = "";
})

.filter('radToDeg', function(){
	return function(input){
		console.log("====> filtering", input)
		return input;
	}
});
