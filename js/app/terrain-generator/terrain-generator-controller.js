angular.module('v3-editor.terrain-generator').controller('NewTerrainCtrl', ['$scope', 'TGBlurFilter',
	function($scope, TGBlurFilter){
		$scope.config = {
			height: 1000,
			width: 1000,
			param: 30,
			filterparam: 0.5,
			depth: 300,
			widthSegments: 25,
			heightSegments: 25,
			filters: [TGBlurFilter]
		};
	}
]);
