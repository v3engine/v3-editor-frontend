'use strict';

angular.module('v3-editor.terrain-generator').service("TerrainGenerator", function() {
	return {
		config: {},
		defaults: {
			height: 500,
			width: 500,
			heightSegments: 250,
			widthSegments: 250,
			param: 1,									//	TODO: rename this
			depth: 5,
			filters: []
		},

		genHeightMap: function(noise){
			var canvas = this.createCanvas(this.config.width, this.config.height);
			var context = canvas.getContext("2d");
			context.save();

			var ratio = this.config.widthSegments / this.config.heightSegments;

			/* Scale random iterations onto the canvas to generate Perlin noise. */
			for( var size = 4; size <= noise.height; size *= this.config.param ){
				var x = ( Math.random() * ( noise.width - size ) ) | 0,
					y = ( Math.random() * ( noise.height - size ) ) | 0;
				context.globalAlpha = 4 / size;
				context.drawImage( noise, Math.max( x, 0 ), y, size * ratio, size, 0, 0, this.config.widthSegments, this.config.heightSegments );
			}
			context.restore();
			return canvas;
		},

		genNoise: function(){
			var canvas = this.createCanvas(this.config.widthSegments, this.config.heightSegments);
			var context = canvas.getContext('2d');
			var imageData = context.getImageData( 0, 0, canvas.width, canvas.height );
			var pixels = imageData.data;
			for( var i = 0; i < pixels.length; i += 4 ){
				pixels[i] = pixels[i+1] = pixels[i+2] = ( Math.random() * 256 ) | 0;
				pixels[i+3] = 255;
			}
			context.putImageData( imageData, 0, 0 );
			return canvas;
		},

		generate: function(params){
			_.assign(this.config, this.defaults, params);
			var noise = this.genNoise();
			var heightMap = this.genHeightMap(noise);

			var geometry = new THREE.BufferGeometry();
			var points = noise.width * noise.height;
			var indices = ( noise.width - 1 ) * ( noise.height - 1 ) * 2 * 3 ;

			for (let i in this.config.filters){
				this.config.filters[i].run(noise, params);
			}

			geometry.addAttribute( 'index', new THREE.BufferAttribute(new Uint32Array( indices ), 1) );
			geometry.addAttribute( 'position', new THREE.BufferAttribute(new Float32Array( points * 3 ), 3) );

			this.createVertices(geometry, noise, 1);
			this.createFaces(geometry);
			return geometry;
		},

		createFaces: function(geometry){
			var indices = geometry.getIndex( 'index' ).array;
			var id = 0;
			console.log(this.config);
			for( var y = 0; y < this.config.heightSegments - 1; ++y )
			{
				for( var x = 0; x < this.config.widthSegments - 1; ++x )
				{
					// First triangle
					indices[id ++] = y * this.config.widthSegments + x + 1;
					indices[id ++] = y * this.config.widthSegments + x;
					indices[id ++] = ( y + 1 ) * this.config.widthSegments + x;

					// Second triangle
					indices[id ++] = ( y + 1 ) * this.config.widthSegments + x + 1;
					indices[id ++] = y * this.config.widthSegments + x + 1;
					indices[id ++] = ( y + 1 ) * this.config.widthSegments + x;
				}
			}
		},

		createVertices: function(geometry, noise){
			var positions = geometry.getAttribute( 'position' ).array;
			var context = noise.getContext('2d'),
				imgData = context.getImageData( 0, 0, noise.width, noise.height ),
				pixels = imgData.data,
				scaleX = this.config.width / ( noise.width - 1 ),
				scaleY = this.config.depth / 255,
				scaleZ= this.config.height / ( noise.height - 1 ),
				id = 0,
				pixel = 0,
				offsetX = - noise.width / 2,
				offsetZ = - noise.height / 2;

			for ( var y = 0; y < noise.height; ++y ) {
				for ( var x = 0; x < noise.width; ++x ) {
					positions[id ++] = scaleX * ( x + offsetX );
					positions[id ++] = scaleY * ( pixels[ (pixel ++) * 4 + 1] );
					positions[id ++] = scaleZ * ( y + offsetZ );
				}
			}
		},

		createCanvas: function(){
			var canvas = document.createElement( "canvas" );
			canvas.width = this.config.widthSegments;
			canvas.height = this.config.heightSegments;
			return canvas;
		},
	}
})
