angular.module('v3-editor.directives')
.directive('v3Viewport', ['Editor',
  function(Editor) {
    return {
    	scope:{
    		system: '='
    	},
      link: function($scope, element, attrs){
        element.append($scope.system.renderer.domElement);
        $scope.system.setSize();
        $scope.mouseUp = new THREE.Vector2();
        $scope.mouseDown = new THREE.Vector2();
        element.bind('mouseup', (event) => {
          event.preventDefault();
          $scope.mouseUp.x = ( event.offsetX / $scope.system.renderer.domElement.width ) * 2 - 1
          $scope.mouseUp.y = - ( event.offsetY / $scope.system.renderer.domElement.height ) * 2 + 1
          if ($scope.mouseUp.distanceTo($scope.mouseDown) == 0){
            var objects = Editor.raycast($scope.mouseUp, Editor.camera);
            if (objects.length) {
              Editor.selectObject(objects[0].object);
              $scope.$apply();
            }
          }
        });
        element.bind('mousedown', (event) => {
          event.preventDefault();
          $scope.mouseDown.x = ( event.offsetX / $scope.system.renderer.domElement.width ) * 2 - 1;
          $scope.mouseDown.y = - ( event.offsetY / $scope.system.renderer.domElement.height ) * 2 + 1;
        });
      }
    };
  }
]);
