angular.module('v3-editor.directives').directive('odValue', [
  function() {
    return {
      restruct: 'E',
      templateUrl: 'views/directives/od-value.html',
      scope: {
        ngModel: '='
      },
      link: function($scope, element, attrs){
        $scope.label = attrs.label;
        $scope.type = attrs.type ? attrs.type : 'text';
      }
    };
  }
]);
