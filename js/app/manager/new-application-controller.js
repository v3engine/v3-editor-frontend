angular.module('v3-editor.manager')
.controller('NewApplicationCtrl', [
  '$scope', 'Editor', '$location', '$rootScope',
  function($scope, Editor, $location, $rootScope) {

    $rootScope.title = "New Application";
    $scope.application = {
      name: '',
      description: ''
    };

    $scope.create = function() {
      var application = Editor.newApplication($scope.application);
      $location.path("/application/" + application.id);
    };
  }]
);
