'use strict';

angular.module('v3-editor').provider("Editor", function() {
	var useStorage = null;

	this.setStorage = function(storage) {
		return useStorage = storage;
	};

	this.$get = function($injector, EditorPrimitives, $rootScope){
		return {
			storage: $injector.get(useStorage),
			settings: {
        nextApplicationID: 0
      },
      selectedObject: {},
      application: null,
      scene: null,		//	current scene
      camera: null,
      isInitialized: false,

			init: function(){
				V3.init({
			    showAxis: true,
			    renderer: {
			    	antialias: true,
			    	shadowMap:{
			    		enabled: false,
			    		needsUpdate: true,
			    		type: THREE.PCFShadowMap
			    	}
			    }
			  });
				this.storage.init();
				_.assign(this.settings, this.storage.getSettings());

				this.sceneHelpers = new THREE.Scene();

				this.selectionBox = new THREE.BoxHelper();
				this.selectionBox.material.depthTest = false;
				this.selectionBox.material.transparent = true;
				this.selectionBox.visible = false;
				this.sceneHelpers.add(this.selectionBox);

				V3.ESManager.init();
				this.renderSystem = V3.ESManager.addSystem(V3.RenderSystem);
        this.renderSystem.camera = this.initCamera();

				new THREE.OrbitControls(this.camera, this.renderSystem.renderer.domElement);
				this.transformControls = new THREE.TransformControls(this.camera, this.renderSystem.renderer.domElement);
				this.sceneHelpers.add(this.transformControls);

				window.addEventListener('keydown', (event) => {
					if (this.transformControls.visible)
		        switch (event.keyCode) {
		          case 87:
		            return this.transformControls.setMode("translate");
		          case 69:
		            return this.transformControls.setMode("rotate");
		          case 82:
		            return this.transformControls.setMode("scale");
		          case 107:
		            return this.transformControls.setSize(this.transformControls.size + 0.1);
		          case 109:
		            return this.transformControls.setSize(Math.max(this.transformControls.size - 0.1, 0.1));
		        }
	      });

				V3.ESManager.run();
				this.isInitialized = true;
			},

			initCamera: function(){
				this.camera = new THREE.PerspectiveCamera(45, 1000 / 100000, 1, 1000000);
        this.camera.position.set(10, 100, 10);
        this.camera.rotation.z = THREE.Math.degToRad(15);
        this.camera.rotation.x = THREE.Math.degToRad(15);
        this.camera.rotation.y = THREE.Math.degToRad(15);
				return this.camera;
			},

			foo: function(path){
      	// spawner
      	var jsonLoader = new THREE.JSONLoader();
      	jsonLoader.load(path, ( geometry, materials ) => {
      		if (materials) {
	        	materials.map((material) => {
	        		material.shading = THREE.FlatShading;
	        	});

      		}
					var material = new THREE.MeshFaceMaterial(materials);

  				for (var i = 0; i<100; i++){
   					var mesh = new THREE.Mesh(geometry, material);
  					var width = 1000;
  					var length = 1000;
  					mesh.position.x = Math.random()*width-width/2;
  					mesh.position.z = Math.random()*length - length/2;
  					var scale = Math.random()*10+1;
  					mesh.scale.x = scale;
  					mesh.scale.y = scale;
  					mesh.scale.z = scale;
	    			mesh.receiveShadow = true;
	    			mesh.castShadow = true;
   					this.scene.add(mesh);
  				}
        });
      },

			addFoliage: function(landscape, items){
				var jsonLoader = new THREE.JSONLoader();
				console.log(items);
				var foliage = new THREE.Object3D();
				for (let i in items){
					jsonLoader.load(items[i].url, ( geometry, materials ) => {
						if (materials) {
		        	materials.map((material) => {
		        		material.shading = THREE.FlatShading;
		        	});
	      		}
	      		var material = new THREE.MeshFaceMaterial(materials);
	     			geometry.center()
						geometry.applyMatrix(new THREE.Matrix4().makeTranslation(0, -geometry.boundingBox.min.y, 0))
	      		for (let j = 0; j<items[i].count; j++){
	   					var mesh = new THREE.Mesh(geometry, material);
	  					var width = 8500;
	  					var length = 8500;
	  					mesh.position.x = Math.random()*width-width/2;
	  					mesh.position.z = Math.random()*length - length/2;
	  					mesh.position.y = 1000;
	  					var down = mesh.position.clone().setY(-10000);
	  					var up = mesh.position.clone().setY(10000);
	  					// var lineGeometry = new THREE.Geometry();
	  					// // lineGeometry.vertices.push(down);
					   //  lineGeometry.vertices.push(mesh.position);
					   //  lineGeometry.vertices.push(up);
					   //  var lineMaterial = new THREE.LineBasicMaterial({
					   //      color: 0x0000ff
					   //  });
	  					// var ray = new THREE.Line(lineGeometry, lineMaterial);
	  					// this.scene.add(ray);
	  					var intersects = this.raycast(mesh.position, down, [landscape]);
	  					if (intersects.length){
	  						// for (let t in intersects){
	  						// 	var is = EditorPrimitives.create('mesh.sphere', {
	  						// 		radius: 10,
	  						// 		color: 0xffffff
	  						// 	});
	  						// 	is.position.copy(intersects[t].point);
	  						// 	this.scene.add(is);
	  						// }
	  						mesh.position.copy(intersects[0].point);
	  					} else {
	  						var intersects = this.raycast(mesh.position, up, [landscape]);
	  						if (intersects.length){
	  							// for (let t in intersects){
		  						// 	var is = EditorPrimitives.create('mesh.sphere', {
		  						// 		radius: 10,
		  						// 		color: 0xffffff
		  						// 	});
		  						// 	is.position.copy(intersects[t].point);
		  						// 	this.scene.add(is);
		  						// }
	  							mesh.position.copy(intersects[0].point);
	  						} else {
	  							// console.log ("fuck");
	  						}
	  					}



	  					var scale = Math.random()*4+1;
	  					mesh.rotation.y = THREE.Math.degToRad(Math.random()*180);
	  					mesh.scale.x = scale;
	  					mesh.scale.y = scale;
	  					mesh.scale.z = scale;
		    			mesh.receiveShadow = true;
		    			mesh.castShadow = true;
	   					foliage.add(mesh);
	   					foliage.name = 'Foliage';
	  				}
					});
				}
				this.scene.add(foliage);
			},

			addLandscape: function(_material) {
				var geometry = new THREE.PlaneGeometry( 1000, 1000, 100, 100 );
				var material = new THREE.MeshPhongMaterial( {color: 0x663300, side: THREE.DoubleSide} );
				material.shininess = 50;
				material.side = 0;
				material.specular.b = 0.4980392156862745;
				material.specular.g = 0.4980392156862745;
				material.specular.r = 0.4980392156862745;
				// material.color.g = 0.07058823529411765;
				// material.color.r = 0.01568627450980392;

				material.shading = THREE.FlatShading;
				// material.wireframe = true;
				console.log(material, _material);
				var landscape = new THREE.Mesh( geometry, material );
				this.scene.add( landscape );
				landscape.rotation.x = THREE.Math.degToRad(-90)
				var height = 10;
				for (var i=1; i<landscape.geometry.vertices.length/1; i+=1){
					landscape.geometry.vertices[i].z = Math.random()*height-height/2;
				}
				var ggg = 3;
				// for (var i=0; i<landscape.geometry.vertices.length; i+=ggg){
				// 	var prev = i>ggg ? landscape.geometry.vertices[i-ggg].z : 0;
				// 	// console.log();
				// 	// console.log((asd ? 1 : -1)*height);
				// 	var asd = Math.round(Math.random()*10);
				// 	var a = (asd>3 ? 1 : -1)*(height+prev);
				// 	// console.log(asd, a);
				// 	landscape.geometry.vertices[i].z = a;
				// }
				// var prev = 0;
				// for (var i=0; i<landscape.geometry.faces.length; i+=4){
				// 	var vertex1 = landscape.geometry.vertices[landscape.geometry.faces[i].a];
				// 	// var vertex2 = landscape.geometry.vertices[landscape.geometry.faces[i].b];


				// 	//  = i>ggg ? landscape.geometry.vertices[i-ggg].z : 0;
				// 	// console.log();
				// 	// console.log((asd ? 1 : -1)*height);
				// 	var asd = Math.round(Math.random());
				// 	prev = (asd ? 1 : -1)*(height+prev);
				// 	// console.log(asd, a);
				// 	vertex1.z = prev;
				// }
				landscape.receiveShadow = true;
	    	landscape.castShadow = true;
	    	console.log(landscape);
	    	// var light = EditorPrimitives.create('light.spot');
	    	// light.position.y = 1000;
	    	// console.log(light);
	    	// this.scene.add(light);
			},

			saveSettings: function() {
        this.storage.saveSettings(this.settings);
      },

			/* save current scene of current application */
			save: function(){
				console.log("saving...", this.scene);
				var json = this.scene.toJSON();
				console.log(json);
      	this.application.scenes[this.application.defaultScene] = json;
      	this.storage.updateApplication(this.application);
			},

			newApplication: function(appData) {
        var application = new V3.Application(appData);
        _.assign(application, {
          id: this.settings.nextApplicationID++,
          dateCreated: new Date(),
          scenes: {}
        });
        var defaultScene = new THREE.Scene();
        application.scenes[application.defaultScene] = defaultScene.toJSON();
        this.storage.newApplication(application);
        this.saveSettings();
        return application;
      },

			openApplication: function(applicationId){
				if (!this.isInitialized) this.init();
				var appData = this.storage.getApplicationById(applicationId);
				var loader = new THREE.ObjectLoader();
				var sceneData = appData.scenes[appData.defaultScene];
				console.log("===>", sceneData);


				if (sceneData.metadata) {		//	very simple way to verify validity of scene data
          this.scene = loader.parse(sceneData);

          this.renderSystem.resetScenes();
          this.renderSystem.addScene(this.scene);
          this.renderSystem.addScene(this.sceneHelpers);
        } else {
        	// what???
        }
        this.application = appData;
        // this.addLandscape(this.scene.children[this.scene.children.length-10].material.materials[3]);
				return appData;
			},

			removeApplication: function(application) {
        this.storage.removeApplication(application);
      },


      addObject: function(object) {
        this.scene.add(object);
      },

			removeObject: function(object) {
        this.scene.remove(object);
      },

			selectObject: function(object) {
				// if selected object is same as before - then deselect it
				if (!object || (this.selectedObject && this.selectedObject.uuid === object.uuid)) {
					this.selectedObject = null;
		      this.selectionBox.visible = false;
		      this.transformControls.detach();
				} else {
					this.selectedObject = object;
					if (object.geometry !== null) {
		        this.selectionBox.update(object);
		        this.selectionBox.visible = true;
		      }
		      this.transformControls.attach(object);
				}
				$rootScope.$broadcast('object_selection', this.selectedObject);
			},
			raycast: function(from, to, objects){
				if (typeof objects === 'undefined') objects = this.scene.children;
				var raycaster = new THREE.Raycaster();
				raycaster.set(from, to.clone().normalize());
				return raycaster.intersectObjects(objects);;
			}
		}
	}
});
