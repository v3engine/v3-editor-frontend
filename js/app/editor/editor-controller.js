'use strict';

angular.module('v3-editor')
.controller('EditorCtrl', [
	'$rootScope', '$scope', '$state', 'Editor', 'EditorPrimitives', 'TerrainGenerator', 'ngDialog',
	function($rootScope, $scope, $state, Editor, EditorPrimitives, TerrainGenerator, ngDialog) {
		$scope.selectedObject = {};
		$rootScope.$on('object_selection', (event, object) => {
			$scope.selectedObject = object;
		});
		var application = Editor.openApplication($state.params.app_id);
		if (application){
			$rootScope.title = application.name;
			$scope.renderSystem = V3.ESManager.getSystem('render');
		}

		$scope.export = () => {
			var json = JSON.stringify(Editor.scene.toJSON());
			var blob = new Blob([json], {type: "application/json;charset=utf-8"});
			saveAs(blob, "scene.json");
		}

		$scope.save = () => {
      Editor.save();
		}

		$scope.addObject = (objectType) => {
			var object = EditorPrimitives.create(objectType);
			if (object) {
		    Editor.addObject(object);
		    Editor.save();
			}
	  }

	  $scope.addLandscape = () => {
	  	ngDialog.open({
        template: 'views/editor/new-terrain.html',
        controller: 'NewTerrainCtrl'
      })
      .closePromise.then(function(data) {
        if (typeof data.value === 'object') {
			  	var terrainGeometry = TerrainGenerator.generate(data.value);
					console.log(terrainGeometry);
					// var terrainMaterial = new THREE.MeshPhongMaterial({
					// 	vertexColors: THREE.VertexColors,
					// 	shading: THREE.FlatShading
					// });
					var terrainMaterial = new THREE.MeshPhongMaterial( {color: 0x663300, side: THREE.DoubleSide} );
					terrainMaterial.shininess = 3;
					terrainMaterial.side = 0;
					terrainMaterial.specular.b = 0.4980392156862745;
					terrainMaterial.specular.g = 0.4980392156862745;
					terrainMaterial.specular.r = 0.4980392156862745;
					// material.color.g = 0.07058823529411765;
					// material.color.r = 0.01568627450980392;

					terrainMaterial.shading = THREE.FlatShading;
					var terrain = new THREE.Mesh( terrainGeometry, terrainMaterial );
					terrain.name = 'Landscape';
					terrain.position.y -= data.value.depth/2;
					Editor.addObject(terrain);
        }
      });
	  }

	  $scope.addFoliage = () => {
	  	var landscape = _.find(Editor.scene.children, {name: "Landscape"});
	  	console.log(landscape);
	  	Editor.addFoliage(landscape, [
	  	{
  			url: 'tree.json',
  			count: 1000
  		},
  		{
  			url: 'spruce.json',
  			count: 1000
  		}
  		]);
	  }

	  // $scope.addFoliage();
	}
]);
