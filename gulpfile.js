var gulp = require('gulp'),
	concat = require('gulp-concat'),
	watch = require('gulp-watch'),
	sass = require('gulp-ruby-sass');

gulp.task('scripts', function() {
  return gulp.src([
  		'js/app/editor/editor-module.js',
  		'js/app/**/**-module.js',
  		'js/app/**/*.js'
  	])
    .pipe(concat('v3-editor.js'))
    .pipe(gulp.dest('build'));
});

gulp.task('styles', function(){
	return sass('sass/style.sass')
		.on('error', sass.logError)
		.pipe(gulp.dest('css'));
})

gulp.task('default', function() {
  gulp.watch('sass/*.sass', ['styles']);

  gulp.watch('js/app/**/*.js', function(event){
  	gulp.run('scripts');
  })
});
