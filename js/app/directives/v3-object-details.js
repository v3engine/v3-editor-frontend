angular.module('v3-editor.directives').directive('v3ObjectDetails', [
  function() {
    return {
      templateUrl: 'views/object-details.html',
      scope: {
        object: '='
      },
      controller: function($scope) {}
    };
  }
]);
