'use strict';

angular.module('v3-editor.storages')
.service('EditorLocalStorage', [
	'localStorageService', '$filter',
	function(localStorageService, $filter) {
		return {
			init: function(){
				if (this.getApplications() === null) {
          localStorageService.set('applications', []);
        }
        if (this.getSettings() === null) {
          return localStorageService.set('settings', {});
        }
			},

      reset: function(){
        localStorageService.clearAll();
        return this.init();
      },

			getSettings: function() {
        return localStorageService.get('settings');
      },

      saveSettings: function(settings) {
        localStorageService.set('settings', settings);
      },

			getApplications: function() {
        return localStorageService.get('applications');
      },

      getApplicationById: function(id) {
        return $filter('filter')(this.getApplications(), {
          id: id
        })[0];
      },

      newApplication: function(application) {
        var applications = this.getApplications();
        applications.push(application);
        localStorageService.set('applications', applications);
      },

      updateApplication: function(application) {
        var applications;
        applications = this.getApplications();
        applications[_.findIndex(applications, {
          id: application.id
        })] = application;
        var a = localStorageService.set('applications', applications);
        console.log(a);
      },

      removeApplication: function(application){
        var applications;
        applications = this.getApplications();
        applications = _.reject(applications, {
          id: application.id
        });
        localStorageService.set('applications', applications);
      }
		}
	}
]);
